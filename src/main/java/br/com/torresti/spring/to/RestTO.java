package br.com.torresti.spring.to;

import br.com.torresti.spring.util.SituacaoRest;

public class RestTO {

	private Long codigo;

	private String mensagem;
	
	public RestTO() {
		this.codigo = SituacaoRest.Ok.getCodigo();
	}
	
	public RestTO(Long codigo, String mensagem) {
		this.codigo = codigo;
		this.mensagem = mensagem;
	}
	
	public static RestTO sucesso() {
		return new RestTO();
	}
	
	public static RestTO sucesso(String mensagem) {
		return new RestTO(SituacaoRest.Ok.getCodigo(), mensagem);
	}
	
	public static RestTO erro(Exception e) {
		return new RestTO(SituacaoRest.Erro.getCodigo(), e.getMessage());
	}
	
	public static RestTO erro(String mensagem) {
		return new RestTO(SituacaoRest.Erro.getCodigo(), mensagem);
	}
	
	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

}
