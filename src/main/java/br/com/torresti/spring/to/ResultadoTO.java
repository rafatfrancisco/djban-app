package br.com.torresti.spring.to;

public class ResultadoTO<T> extends RestTO {

	private T resultado;
	
	public T getResultado() {
		return resultado;
	}

	public void setResultado(T resultado) {
		this.resultado = resultado;
	}
	
}
