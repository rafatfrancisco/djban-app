package br.com.torresti.musica.service.vo;

public class ParticipanteVO {

	private String codigoPromocao;

	private String nome;

	private String cpf;

	private String email;

	private String celular;
	
	public ParticipanteVO() {}
	
	public ParticipanteVO(String codigoPromocao) {
		this.codigoPromocao = codigoPromocao;		
	}

	public String getCodigoPromocao() {
		return codigoPromocao;
	}

	public void setCodigoPromocao(String codigoPromocao) {
		this.codigoPromocao = codigoPromocao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

}
