package br.com.torresti.musica.service.view;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.torresti.musica.service.util.MenuTela;

@Controller
@RequestMapping(value = "")
public class HomeView {
	
	@RequestMapping(value = "/")
	public ModelAndView acessarPromocao(HttpServletRequest request) throws Exception {
		ModelAndView modelAndView = new ModelAndView("jsp/index");
		modelAndView.getModelMap().put("tela", MenuTela.PromocaoCarregar.getOpcao());
		return modelAndView;
	}
	
	@RequestMapping(value = "/{codigo}")
	public ModelAndView acessarParticipante(HttpServletRequest request, @PathVariable String codigo) throws Exception {
		ModelAndView modelAndView = new ModelAndView("jsp/index");
		modelAndView.getModelMap().put("tela", MenuTela.PromocaoParticipar.getOpcao() + "/" + codigo);
		return modelAndView;
	}
	
}
