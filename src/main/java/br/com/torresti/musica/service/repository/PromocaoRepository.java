package br.com.torresti.musica.service.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.torresti.musica.crud.model.entidy.Promocao;

@Repository
public interface PromocaoRepository extends CrudRepository<Promocao, Integer> {

	@Query("SELECT p FROM Promocao p ")
	public List<Promocao> listar();
	
	@Query("SELECT p FROM Promocao p WHERE p.codigo = ?1")
	public Promocao consultar(String codigo);
	
}
