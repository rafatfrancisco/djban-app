package br.com.torresti.musica.service.component;

import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.torresti.musica.crud.model.entidy.Participante;
import br.com.torresti.musica.crud.model.entidy.Promocao;
import br.com.torresti.musica.crud.model.entidy.PromocaoParticipante;
import br.com.torresti.musica.service.repository.PromocaoParticipanteRepository;
import br.com.torresti.musica.service.repository.PromocaoRepository;
import br.com.torresti.musica.service.vo.ParticipanteVO;
import br.com.torresti.musica.service.vo.PromocaoVO;
import br.com.torresti.musica.util.DateUtil;
import br.com.torresti.spring.to.RestTO;
import br.com.torresti.spring.util.ViewUtil;

@Component
public class PromocaoComponent extends DjBanComponent {
	
	@Autowired
	private ParticipanteComponent participanteComponent;
	
	@Autowired
	private PromocaoRepository promocaoRepository;
	
	@Autowired
	private PromocaoParticipanteRepository promocaoParticipanteRepository;
	
	public RestTO salvar(PromocaoVO promocaoVO) {
		
		try {
		
			ViewUtil.isEmpty(promocaoVO.getCodigo(), "Favor informar código");
			
			ViewUtil.isEmpty(promocaoVO.getDescricao(), "Favor informar descrição");
			
			ViewUtil.isEmpty(promocaoVO.getDataInicio(), "Favor informar data inicio");
			
			ViewUtil.isEmpty(promocaoVO.getHoraInicio(), "Favor informar hora incio");
			
			ViewUtil.isEmpty(promocaoVO.getDataFim(), "Favor informar data fim");
			
			ViewUtil.isEmpty(promocaoVO.getHoraFim(), "Favor informar hora fim");
			
			ViewUtil.isEmpty(promocaoVO.getDataSorteio(), "Favor informar data sorteio");
			
			ViewUtil.isNotNull(promocaoRepository.consultar(promocaoVO.getCodigo()), "Já existe uma promoção com esse código");
			
			Promocao promocao = new Promocao();
			
			BeanUtils.copyProperties(promocaoVO, promocao);
			
			Date inicio = DateUtil.parseData(promocaoVO.getDataInicio(), promocaoVO.getHoraInicio());
			
			Date fim = DateUtil.parseData(promocaoVO.getDataFim(), promocaoVO.getHoraFim());
			
			ViewUtil.isTrue(inicio.after(fim), "Data de inicio maior que data de fim");
			
			promocao.setInicio(inicio);
			
			promocao.setFim(fim);
			
			promocao.setSorteio(DateUtil.parseData(promocaoVO.getDataSorteio()));
			
			entityManager.persist(promocao);
				
			return RestTO.sucesso();
			
		} catch (Exception e) {
			
			 return RestTO.erro(e);
			
		}
		
	}
	
	public List<Promocao> listar() {
		
		return promocaoRepository.listar();
		
	}
	
	public PromocaoVO consultar(Integer idPromocao) {
		
		Promocao promocao = entityManager.find(Promocao.class, idPromocao);
		
		PromocaoVO promocaoVO = new PromocaoVO();
		
		BeanUtils.copyProperties(promocao, promocaoVO);
		
		promocaoVO.setDataInicio(DateUtil.formataSemHora(promocao.getInicio()));
		
		promocaoVO.setHoraInicio(DateUtil.formataHoraMinuto(promocao.getInicio()));
				
		promocaoVO.setDataFim(DateUtil.formataSemHora(promocao.getFim()));
		
		promocaoVO.setHoraFim(DateUtil.formataHoraMinuto(promocao.getFim()));
		
		promocaoVO.setDataSorteio(DateUtil.formataSemHora(promocao.getSorteio()));
				
		return promocaoVO;
		
	}	
	
	public synchronized RestTO incluirParticipante(ParticipanteVO participanteVO) {
		
		try {
			
			Promocao promocao = promocaoRepository.consultar(participanteVO.getCodigoPromocao());
			
			ViewUtil.isNull(promocao, "Codigo de promocao invalido");
		
			List<PromocaoParticipante> listaPromocaoParticipantes = promocaoParticipanteRepository.lista(promocao.getIdPromocao());
			
			ViewUtil.isTrue(listaPromocaoParticipantes != null && listaPromocaoParticipantes.size() == promocao.getQuantidade(), "Numero máximo de inscritos nessa promoção");
			
			Date dataAtual = new Date();
			
			ViewUtil.isTrue(promocao.getInicio().after(dataAtual), "Promocao não iniciou");
			
			ViewUtil.isTrue(dataAtual.after(promocao.getFim()), "Promocao finalizada");
			
			for (PromocaoParticipante promocaoParticipante : listaPromocaoParticipantes) {
				
				ViewUtil.isTrue(promocaoParticipante.getParticipante().getCpf().equals(participanteVO.getCpf()), "Cpf já esta participando dessa promoção");
				
			}
					
			Participante participante = participanteComponent.incluir(participanteVO);
			
			Integer numero = 1;
			
			if (listaPromocaoParticipantes != null && listaPromocaoParticipantes.size() != 0) {
				
				numero = listaPromocaoParticipantes.size();
				
			}
			
			PromocaoParticipante promocaoParticipante = new PromocaoParticipante();
			
			promocaoParticipante.setPromocao(promocao);
			
			promocaoParticipante.setParticipante(participante);
			
			promocaoParticipante.setData(dataAtual);
			
			promocaoParticipante.setNumero(numero);
			
			entityManager.persist(promocaoParticipante);			
			
			return RestTO.sucesso("Participante incluido com sucesso!");
			
		} catch (Exception e) {
			
			 return RestTO.erro(e);
			
		}
		
		
	}

	public RestTO alterar(PromocaoVO promocaoVO) {
		
		try {
		
			ViewUtil.isEmpty(promocaoVO.getCodigo(), "Favor informar código");
			
			ViewUtil.isEmpty(promocaoVO.getDescricao(), "Favor informar descrição");
			
			ViewUtil.isEmpty(promocaoVO.getDataInicio(), "Favor informar data inicio");
			
			ViewUtil.isEmpty(promocaoVO.getHoraInicio(), "Favor informar hora incio");
			
			ViewUtil.isEmpty(promocaoVO.getDataFim(), "Favor informar data fim");
			
			ViewUtil.isEmpty(promocaoVO.getHoraFim(), "Favor informar hora fim");
			
			ViewUtil.isEmpty(promocaoVO.getDataSorteio(), "Favor informar data sorteio");
			
			Promocao promocao = promocaoRepository.consultar(promocaoVO.getCodigo());
			
			BeanUtils.copyProperties(promocaoVO, promocao);
			
			Date inicio = DateUtil.parseData(promocaoVO.getDataInicio(), promocaoVO.getHoraInicio());
			
			Date fim = DateUtil.parseData(promocaoVO.getDataFim(), promocaoVO.getHoraFim());
			
			ViewUtil.isTrue(inicio.after(fim), "Data de inicio maior que data de fim");
			
			promocao.setInicio(inicio);
			
			promocao.setFim(fim);
			
			promocao.setSorteio(DateUtil.parseData(promocaoVO.getDataSorteio()));
			
			entityManager.merge(promocao);
			
			return RestTO.sucesso();
			
		} catch (Exception e) {
			
			 return RestTO.erro(e);
			
		}			
		
	}
		
	public RestTO premiar(Integer idPromocao, Integer numero) {
		
		try {
		
			PromocaoParticipante promocaoParticipante = promocaoParticipanteRepository.consultar(idPromocao, numero).get(0);
			
			ViewUtil.isNull(promocaoParticipante, "Promocao e participante nao localizado");
			
			Promocao promocao = promocaoParticipante.getPromocao();
			
			ViewUtil.isNotNull(promocao.getNumero(), "Promocao já possui um ganhador");
			
			promocao.setNumero(numero);
			
			entityManager.merge(promocao);
			
			return RestTO.sucesso();
			
		} catch (Exception e) {
			
			 return RestTO.erro(e);
			
		}		
		
	 }

}
