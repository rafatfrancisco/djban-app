package br.com.torresti.musica.service.vo;

import java.util.List;

import br.com.torresti.musica.crud.model.entidy.Promocao;

public class PromocaoVO {

	private String codigo;

	private String nome;

	private String descricao;

	private String dataInicio;

	private String horaInicio;

	private String dataFim;

	private String horaFim;

	private String dataSorteio;

	private Integer quantidade;
	
	private List<Promocao> listaPromocao;
	
	public PromocaoVO() {}
	
	public PromocaoVO(List<Promocao> listaPromocao) {
		this.listaPromocao = listaPromocao;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}

	public String getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}

	public String getDataFim() {
		return dataFim;
	}

	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}

	public String getHoraFim() {
		return horaFim;
	}

	public void setHoraFim(String horaFim) {
		this.horaFim = horaFim;
	}

	public String getDataSorteio() {
		return dataSorteio;
	}

	public void setDataSorteio(String dataSorteio) {
		this.dataSorteio = dataSorteio;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public List<Promocao> getListaPromocao() {
		return listaPromocao;
	}

	public void setListaPromocao(List<Promocao> listaPromocao) {
		this.listaPromocao = listaPromocao;
	}
	
}
