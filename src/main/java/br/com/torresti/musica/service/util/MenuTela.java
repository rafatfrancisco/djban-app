package br.com.torresti.musica.service.util;

public enum MenuTela {

	PromocaoCarregar("promocao", "/carregar", "Promocao", "Cadastro Promoção"),
	PromocaoParticipar("promocao", "/participar", "Promocao", "Cadastro Participante"),
	;
	
	private String view;
	private String tela;
	private String menu;
	private String titulo;

	private MenuTela(String view, String tela, String menu, String titulo) {
		this.view = view;
		this.tela = tela;
		this.menu = menu;
		this.titulo = titulo;
	}
	
	public String getView() {
		return view;
	}

	public String getTela() {
		return tela;
	}
	
	public String getMenu() {
		return menu;
	}

	public String getTitulo() {
		return titulo;
	}
	
	public String getOpcao() {
		return this.view + this.tela;
	}
	
	public String getCaminho() {
		return this.menu + " > " + this.titulo;
	}
	
	public static MenuTela get(String tela)  {
		for (MenuTela menuTela : MenuTela.values()) {
			if (menuTela.getTela().equals(tela)) {
				return menuTela;
			}
		}
		return null;
	}

}
