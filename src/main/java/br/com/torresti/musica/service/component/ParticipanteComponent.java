package br.com.torresti.musica.service.component;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.torresti.musica.crud.model.entidy.Participante;
import br.com.torresti.musica.service.repository.ParticipanteRepository;
import br.com.torresti.musica.service.vo.ParticipanteVO;
import br.com.torresti.spring.exception.BusinessException;
import br.com.torresti.spring.util.ViewUtil;

@Component
public class ParticipanteComponent extends DjBanComponent {

	@Autowired
	private ParticipanteRepository participanteRepository;
	
	public Participante incluir(ParticipanteVO participanteVO) throws BusinessException {
		
		ViewUtil.isNull(participanteVO.getCpf(), "Favor informar CPF");
		
		ViewUtil.isEmpty(participanteVO.getNome(), "Favor informar nome");
		
		ViewUtil.isEmpty(participanteVO.getEmail(), "Favor informar email");
		
		ViewUtil.isNull(participanteVO.getCelular(), "Favor informar celular");
		
		Participante participante = participanteRepository.consultar(participanteVO.getCpf());
		
		boolean novo = false;
		
		if (participante == null) {
			
			participante = new Participante();
			
			novo = true;
			
		}
		
		BeanUtils.copyProperties(participanteVO, participante);
		
		if (novo) {
			
			entityManager.persist(participante);
			
		} else {
			
			entityManager.merge(participante);
			
		}
		
		return participante;
		
	}
	
}
