package br.com.torresti.musica.service.view;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.torresti.musica.crud.model.entidy.Promocao;
import br.com.torresti.musica.service.component.PromocaoComponent;
import br.com.torresti.musica.service.vo.ParticipanteVO;
import br.com.torresti.musica.service.vo.PromocaoVO;
import br.com.torresti.spring.to.RestTO;

@Controller
@RequestMapping(value = "/promocao")
public class PromocaoView {
	
	@Autowired
	private PromocaoComponent promocaoComponent;
	
	@PostMapping(value = "/salvar")
	public ResponseEntity<RestTO> salvar(@RequestBody PromocaoVO promocaoVO) {
		return new ResponseEntity<RestTO>(promocaoComponent.salvar(promocaoVO), HttpStatus.OK);
	}
	
	@GetMapping(value = "/listar")
	public ResponseEntity<List<Promocao>> listar() {
		return new ResponseEntity<List<Promocao>>(promocaoComponent.listar(), HttpStatus.OK);
	}
	
	@PostMapping(value = "/incluir-participante")
	public ResponseEntity<RestTO> incluirParticipante(@RequestBody ParticipanteVO participanteVO) {
		return new ResponseEntity<RestTO>(promocaoComponent.incluirParticipante(participanteVO), HttpStatus.OK);
	}
	
	@PostMapping(value = "/alterar")
	public ResponseEntity<RestTO> alterar(@RequestBody PromocaoVO promocaoVO) {
		return new ResponseEntity<RestTO>(promocaoComponent.alterar(promocaoVO), HttpStatus.OK);
	}
	
	@GetMapping(value = "/premiar/{idPromocao}/{numero}")
	public ResponseEntity<RestTO> premiar(@PathVariable Integer idPromocao, @PathVariable Integer numero) {
		return new ResponseEntity<RestTO>(promocaoComponent.premiar(idPromocao, numero), HttpStatus.OK);
	}


	@GetMapping(value = "/participar/{codigo}")
	public ModelAndView participar(@PathVariable String codigo) {
		return new ModelAndView("jsp/promocao/participar", "participanteVO", new ParticipanteVO(codigo));
	}
	
	@ResponseBody
	@PostMapping(value = "/cadastrar-participante/ajax")
	public RestTO cadastrarParticipanteAjax(@ModelAttribute("participanteVO") ParticipanteVO participanteVO) {
		return promocaoComponent.incluirParticipante(participanteVO);
	}
	
	@GetMapping(value = "/carregar")
	public ModelAndView carregar() {
		return new ModelAndView("jsp/promocao/carregar", "promocaoVO", new PromocaoVO(promocaoComponent.listar()));
	}
	
	@GetMapping(value = "/gerar")
	public ModelAndView gerar() {
		return new ModelAndView("jsp/promocao/modal-promocao", "promocaoVO", new PromocaoVO());
	}
	
	@GetMapping(value = "/consultar/{idPromocao}")
	public ModelAndView consultar(@PathVariable Integer idPromocao) {
		return new ModelAndView("jsp/promocao/modal-promocao", "promocaoVO", promocaoComponent.consultar(idPromocao));
	}
	
	@ResponseBody
	@PostMapping(value = "/salvar/ajax")
	public RestTO salvarAjax(@ModelAttribute("promocaoVO")  PromocaoVO promocaoVO) {
		return promocaoComponent.salvar(promocaoVO);
	}
	
	@ResponseBody
	@PostMapping(value = "/alterar/ajax")
	public RestTO alterarAjax(@ModelAttribute("promocaoVO")  PromocaoVO promocaoVO) {
		return promocaoComponent.alterar(promocaoVO);
	}
	
	@ResponseBody
	@GetMapping(value = "/premiar/ajax/{idPromocao}/{numero}")
	public RestTO premiarAjax(@PathVariable Integer idPromocao, @PathVariable Integer numero) {
		return promocaoComponent.premiar(idPromocao, numero);
	}


}
