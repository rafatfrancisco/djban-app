package br.com.torresti.musica.service.util;

import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.ConstructorUtils;

/**
 * Classe utilit�ria para opera��es de reflection
 */
@SuppressWarnings({"rawtypes", "unchecked"})
public class ReflectionUtil {

	/**
	 * Retorna o valor do campo. Se o campo for public retorna o valor de forma direta <br>
	 * caso n�o o seja tenta invocar o m�todo get do campo.
	 *
	 */
	public static Object getValueByField(Field field, Object target) {
		try{
		   if (field.getModifiers() == Modifier.PUBLIC) {
            return field.get(target);
        }

		   Method getter = getGetterMethod(target.getClass(), field.getName(), true);
		   if (getter != null) {
            return getter.invoke(target);
        }

		   makeAttributesAccessible(field);
		   return field.get(target);
		}catch(Exception ex){
			throw new RuntimeException("Error accessing ["+field.getName()+"] in ["+target.getClass()+"]",ex);
		}
	}

	public static void setValueByField(Field field, Object target, Object value){
		Method setterMethod = null;
		if (value != null) {
            setterMethod = getSetterMethod(target.getClass(), field.getName(), value.getClass(), true);
        }

		if (setterMethod != null){
		   try {
		      setterMethod.invoke(target, value);
		   } catch (Exception e) {
		      throw new RuntimeException("Error invoking setter method for "+field.getName()+" in "+target.getClass(),e);
		   }
		}
		else{
		   try {
			  makeAttributesAccessible(field);
			  field.set(target, value);
		   } catch (IllegalArgumentException ex) {
			  throw new RuntimeException(ex);
		   } catch (IllegalAccessException ex) {
			  throw new RuntimeException(ex);
		   }
	   }
	}

	/**
	 * Retorna o nome do m�todo get do campo passado como argumento<br><br>
	 *
	 * Se o campo for por exemplo name ent�o a String retornada ser� getName.
	 *
	 * Obs:
	 */
	public static String getGetterMethodName(Field field){
		return getGetterMethodName(field.getName());
	}

	public static String getGetterMethodName(String fieldName){
		return getMethod("get", fieldName);
	}

	public static String getSetterMethodName(String fieldName){
		return getMethod("set", fieldName);
	}

	private static String getMethod(String prefix,String fieldName){
		if (StringUtils.startsWith(fieldName, prefix)) {
            return fieldName;
        }

		String first = fieldName.substring(0,1);
		return prefix+fieldName.replaceFirst(first, first.toUpperCase());
	}

	public static Method getGetterMethod(Object tager,String methodOrPropertyName,boolean findInSuperClasses){
	   return getGetterMethod(tager.getClass(), methodOrPropertyName, findInSuperClasses);
	}

	public static Method getGetterMethod(Class clazz,String methodOrPropertyName,boolean findInSuperClasses){
		methodOrPropertyName = getGetterMethodName(methodOrPropertyName);

		try {
			return clazz.getDeclaredMethod(methodOrPropertyName);
		} catch (Exception ex) {
			if (findInSuperClasses && clazz != Object.class) {
                return getGetterMethod(clazz.getSuperclass(), methodOrPropertyName, findInSuperClasses);
            }
		}

		return null;
	}

	/**
     * Retorna o valor via reflex�o de uma propriedade de um objeto.
     *
     * Este m�todo d� prioridade a chamada de m�todos get caso n�o tenha um m�todo definido
     * para a propriedade ent�o a propriedade � setada como acessible = true e acessada diretamente.
     *
     * Este m�todo trabalha com multin�vel de modo que � recuperado qualquer valor de qualquer propriedade
     * mesmo que essa propriedade esteja dentro de um objeto dentro de outro objeto, que es�ta dentro de outro objeto
     * e por ai vai tendo como base o objeto passado como parametro.<br><br>
     *
     * <b>Exemplo de Uso</b><br>
     * Considere o objeto abaixo<br><br>
     * <code>
     *   Pessoa pessoa = new Pessoa[nome = 'joao',endereco[rua = 'ip� roxo', bairro = 'asa norte']]
     *   <br><br>
     *   Exemplos de chamada e resultado:<br>
     *    1 - getValorViaReflexao("nome",pessoa) : Resultado = 'jo�o'<br>
     *    2 - getValorViaReflexao("endereco.bairro",pessoa) : Resultado = 'asa norte' <br><br>
     *  </code>
     *  <b>Obs: Verifique que o exemplo 2 acessa uma propriedade dentro de um objeto que � propriedade do objeto passado como par�metro.
     *          Isso pode ser feito sem limite de n�vel. Levando em considera��o padr�o java bean de propriedades qualquer propriedade
     *          acess�vel a partir do objeto passado como par�metro pode ser acessado seguindo o padr�o
     *          "propriedade1.propriedade2DentroDaPropriedade1.propriedade3DentroDaPropriedade2.etc...".
     *          Da mesma forma passando apenas a propriedade raiz do objeto como no exemplo 1,
     *          o m�todo automaticamente identifica isso e tenta chamar o m�todo get, caso este n�o exista,
     *          tenta acessar a propriedade diretamente.</b>
     *
     *
     * @param property String no padr�o java bean da propriedade ao qual se deseja o valor no objeto
     * passado como argumento para o parametro 'objeto' deste m�todo
     * @param target objeto onde a propriedade ser� buscada e retornada
     * @return o valor retornado pelo m�todo get da propriedade ou o pr�prio valor direto da propriedade caso um m�todo get para a mesma n�o tenha sido definido.
     *
     * @author Victor Lindberg (victorlindberg713@gmail.com)
     */
    public static Object getValueByReflection(String property,Object target){

        String[] propertyTokens = property.split("\\.");

        Field field = null;
        Method getterMethod = null;
        String rootProperty = property;
        boolean isMultLevelProperty = property.contains(".");
        if (! isMultLevelProperty){
            getterMethod = getGetterMethod(target.getClass(), property, true);
            field = getField(target.getClass(),property,true);
        }
        else{
            rootProperty =  propertyTokens[0];
            property = StringUtils.removeStart(property, rootProperty+".");
            getterMethod = getGetterMethod(target.getClass(), rootProperty, true);
            field = getField(target.getClass(),rootProperty,true);
        }

        if (getterMethod == null && field == null) {
            throw new RuntimeException("Property or getter Method (including inherited) not found to ["+property+"] in ["+target.getClass()+"]");
        }

        if (getterMethod != null){
            try{
               Object result = getterMethod.invoke(target);
               if (isMultLevelProperty) {
                return getValueByReflection(property, result);
            } else {
                return result;
            }
            }catch(Exception ex){
               throw new RuntimeException("Error invoking getter method "+getterMethod.getName()+" in "+target.getClass(), ex);
            }
        }

        makeAttributesAccessible(field);
        try {
           Object result = field.get(target);
           if (isMultLevelProperty) {
            return getValueByReflection(property,result);
        } else {
            return result;
        }
        }catch (Exception ex) {
           throw new RuntimeException("Error accessing "+field.getName()+" in "+target.getClass(),ex);
        }

    }

	public static Method getSetterMethod(Object target,String methodOrPropertyName,Class paramClassSet,boolean findInSuperClasses){
		return getSetterMethod(target.getClass(), methodOrPropertyName, paramClassSet,findInSuperClasses);
	}

	public static Method getSetterMethod(Class clazz,String methodOrPropertyName,Class paramClassSet,boolean findInSuperClasses){
		methodOrPropertyName = getSetterMethodName(methodOrPropertyName);

		try {
			return clazz.getDeclaredMethod(methodOrPropertyName,paramClassSet);
		} catch (Exception ex) {
			if (findInSuperClasses && clazz != Object.class) {
                return getSetterMethod(clazz.getSuperclass(), methodOrPropertyName, paramClassSet, findInSuperClasses);
            }
		}

		return null;
	}

	public static void setValueByReflection(String property, Object target,
        Object value) {
		if (value == null){
			Field field = getField(target, property, true);
			makeAttributesAccessible(field);
			setValueByField(field, target, null);
		} else {
            setValueByReflection(property, target, value, value.getClass());
        }
    }

	/**
	 * Seta o valor de uma propriedade no objeto target via reflection.
	 * Function tamb�m para propriedades aninhadas respeitando o padr�o de nomencatura javabean.
	 * Se um m�todo setter existir, o mesmo ser� chamado para efetuar o seto do valor.
	 * Se a propriedade n�o possuir um setter o valor ser� setado diretamente.
	 * <br><br>
	 * <code>Exemplo: setValueByReflection('endereco.cep.numero',pessoa,'12345-110',String.class).</code>
	 * <br>
	 * <b>Resultado:</b> Este m�todo vai setar a propriedade numero dentro de cep dentro
	 * de endereco dentro de perssoa.
	 *
	 * @param property nome da proprieade a ser setada.
	 * @param target objeto alvo
	 * @param value valor a ser setado
	 * @param setParamClass a classe exata do tipo de parametro do m�todo set a ser usado, caso exista um.
	 */
	public static void setValueByReflection(String property, Object target,
        Object value, Class setParamClass) {

        String[] propriedadeTokens = property.split("\\.");

        boolean isMultLevelProperty = property.contains(".");
        if (!isMultLevelProperty) {
            Method setterMethod = getSetterMethod(target.getClass(),
                property, true, setParamClass);
            if (setterMethod != null){
            	try {
            		setterMethod.invoke(target, value);
            	} catch (Exception ex) {
            		throw new RuntimeException(ex);
            	}
            }else{
            	Field field = getField(target.getClass(), property, true);
            	if (field == null) {
                    throw new RuntimeException("Property or setter Method (including inherited) not found to ["+property+"] in ["+target.getClass()+"]");
                }
            	setValueByField(field, target, value);
            }
        } else {
            int lastIndexProperty = StringUtils.lastIndexOf(property,
                ".");
            String propriedadeSet = propriedadeTokens[propriedadeTokens.length - 1];
            property = StringUtils.substring(property, 0,
                lastIndexProperty);
            target = getValueByReflection(property, target);
            setValueByReflection(propriedadeSet, target, value, setParamClass);
        }
    }

    public static Method getSetterMethod(Class clazz,
        String methodOrPropertyName, boolean findInSuperClasses,
        Class... classParams) {
        if (methodOrPropertyName == null) {
            throw new IllegalStateException("methodOrPropertyName is null");
        }

        if (!methodOrPropertyName.startsWith("set")) {
            methodOrPropertyName = getSetterMethodName(methodOrPropertyName);
        }

        try {
            return clazz.getDeclaredMethod(methodOrPropertyName, classParams);
        } catch (Exception ex) {
            if (findInSuperClasses && clazz != Object.class) {
                return getSetterMethod(clazz.getSuperclass(),
                    methodOrPropertyName, findInSuperClasses, classParams);
            }
        }

        return null;
    }

	/**
	 * Retorna o campo de <code>clazz</code> cujo o nome � passado como argumento retornando <br>
	 * null caso este n�o seja encontrado na classe
	 *
	 *
	 * @param clazz
	 * @param fieldName
	 * @return campo com o nome especificado na classe ou null caso este n�o seja encontrado na classe
	 */
	public static Field getField(Class clazz, String fieldName, boolean findInSuperClasses) {
		try {
			return clazz.getDeclaredField(fieldName);
		} catch (NoSuchFieldException e) {
			if (findInSuperClasses && clazz != Object.class){
			      return getField(clazz.getSuperclass(), fieldName, findInSuperClasses);
			}
		}
		return null;
	}

	/**
	 * Retorna o campo de <code>clazz</code> cujo o nome � passado como argumento retornando <br>
	 * null caso este n�o seja encontrado na classe do bean especificado
	 *
	 *
	 * @param target
	 * @param fieldName
	 * @return campo com o nome especificado na classe ou null caso este n�o seja encontrado na classe do bean especificado
	 */
	public static Field getField(Object target, String fieldName, boolean findInSuperClasses) {
			return getField(target.getClass(), fieldName, findInSuperClasses);
	}

	public static void makeAttributesAccessible(Field... fields) {
		for (Field field : fields) {
            field.setAccessible(true);
        }
	}

	/**
     * Efetua o carregamento dos campos da classe em uma lista passada como argumento.
     *
     * @param clazz classe a ser feita o carregamento dos campos
     * @param fieldList lista que receber� o carregamento dos campos
     * @param findInSuperClasses true se deve carregar os campos herdados e false caso contrario
     * @param setFieldsAsAccessible true se cada campo deve ser setado como acessivel e false caso contrario
     */
    public static void loadFields(Class clazz, List<Field> fieldList, boolean findInSuperClasses,
        boolean setFieldsAsAccessible) {
        if (clazz.equals(Object.class)) {
            return;
        }

        for (Field field : clazz.getDeclaredFields()) {

        	if (setFieldsAsAccessible) {
                makeAttributesAccessible(field);
            }
            fieldList.add(field);
        }

        if (findInSuperClasses) {
            loadFields(clazz.getSuperclass(), fieldList, findInSuperClasses, setFieldsAsAccessible);
        }
    }

    /**
     * Obtem os campos [Field] public, private, protected, default de um bean incluindo os herdados.
     *
     * @param target bean.
     * @param findInSuperClasses true se deve carregar os campos herdados e false caso contrario.
     * @return array com os campos declarados na classe correspone ao bean.
     */
    public static Field[] getFields(Object target, boolean findInSuperClasses, boolean setFieldsAsAccessible) {
        if (!(target instanceof Object)) {
            return target.getClass().getDeclaredFields();
        }

        List<Field> campos = new ArrayList<Field>();
        loadFields(target.getClass(), campos, findInSuperClasses, setFieldsAsAccessible);
        return campos.toArray(new Field[campos.size()]);
    }

    /**
     * Chama um m�todo via reflex�o no objeto especifico.
     *
     * @param target objeto onde ser� invocado o m�todo.
     * @param methodName nome do m�todo a ser invocado.
     * @param params parametros do m�todo a ser invocado.
     *
     * @return retorno da execu��o do m�todo. Caso o m�todo seja void � retornado null.
     * @throws NoSuchMethodException se o m�todo em quest�o n�o foi encontrado.
     *
     */
    public static Object callMethodByReflection(Object target, String methodName, Object... params)
        throws NoSuchMethodException {
        if (target == null) {
            throw new IllegalStateException("Objeto n�o pode ser null");
        }

        Class[] paramsTypes = new Class[params.length];
        for (int i = 0; i < paramsTypes.length; i++) {
            paramsTypes[i] = params[i].getClass();
        }

        try {
            Method metodo = getMethod(methodName, target.getClass(), paramsTypes);
            if (metodo == null) {
                throw new NoSuchMethodException("M�todo " + methodName + " com a assinatura buscada n�o encontrado em "
                    + target.getClass() + " e em nenhuma de suas super classes");
            }
            return metodo.invoke(target, params);
        } catch (Exception ex) {
            throw new RuntimeException("Erro acessando metodo " + methodName + " na class " + target.getClass(), ex);
        }
    }

    public static Method getMethod(String methodName, Class clazz, List<Object> args) {
    	return getMethod(methodName, clazz, args.toArray());
    }

    public static Method getMethod(String methodName, Class clazz, Object[] args) {
    	Class[] classes = new Class[args.length];
    	for(int  i = 0; i < args.length;i++) {
            classes[i] = args[i].getClass();
        }

    	return getMethod(methodName, clazz, classes);
    }

    /**
     * Obtem um m�todo especifico em uma classe via reflex�o.<br>
     * Este m�todo busca nas superclasses tamb�m.
     *
     * @param methodName nome do m�todo a ser buscado.
     * @param clazz classe a partir de onde o m�todo ser� buscado.
     * @param paramsTypes parametros do m�todo.
     *
     * @return instancia do m�todo encontrado ou null<br
     *         . caso o m�todo em quest�o com os parametros passados n�o tenha sido encontado.
     */
    public static Method getMethod(String methodName, Class clazz, Class... paramsTypes) {
        Method method = null;
        try {
            method = clazz.getDeclaredMethod(methodName, paramsTypes);
        } catch (SecurityException e) {
            throw new RuntimeException("Erro acessando m�todo", e);
        } catch (NoSuchMethodException e) {
            if (clazz == Object.class) {
                return null;
            }
            return getMethod(methodName, clazz.getSuperclass(), paramsTypes);
        }

        return method;
    }

    /**
     * Verifica se o objeto target possui um determinado atributo.
     *
     * @param target Objeto
     * @param attribute Nome do atributo
     * @return true indica que possui o atributo e false indica que o objeto N�O possui o mesmo
     */
    public static boolean hasAttibute(Object target, String attribute, boolean findInSuperClasses) {
    	return getField(target, attribute, findInSuperClasses) != null;
    }

	public static <T> Class<T> getGenericTypeArgument(final Class<?> clazz, final int idx) {
		final Type type = clazz.getGenericSuperclass();

		ParameterizedType paramType;
		try {
			paramType = (ParameterizedType) type;
		} catch (ClassCastException cause) {
			paramType = (ParameterizedType) ((Class<T>) type).getGenericSuperclass();
		}

		return (Class<T>) paramType.getActualTypeArguments()[idx];
	}

	public static <T> Class<T> getGenericTypeArgument(final Field field, final int idx) {
		final Type type = field.getGenericType();
		final ParameterizedType paramType = (ParameterizedType) type;

		return (Class<T>) paramType.getActualTypeArguments()[idx];
	}

	public static <T> Class<T> getGenericTypeArgument(final Member member, final int idx) {
		Class<T> result = null;

		if (member instanceof Field) {
			result = getGenericTypeArgument((Field) member, idx);
		} else if (member instanceof Method) {
			result = getGenericTypeArgument((Method) member, idx);
		}

		return result;
	}

	public static <T> Class<T> getGenericTypeArgument(final Method method, final int pos) {
		return (Class<T>) method.getGenericParameterTypes()[pos];
	}

	/**
	 * Creates an instance.
	 *
	 * @param <E> bean type expected.
	 * @param clazz bean class.
	 * @param args contructor arguments.
	 * @return bean instance.
	 */
	public static <E> E createInstance(Class<E> clazz,Object... args){
		try{
			return (E) ConstructorUtils.invokeConstructor(clazz, args);
		}catch(Exception ex){
			throw new RuntimeException(ex);
		}
	}

	public static String getClassNameInCallStack(int callIndex){
		Exception ex = new Exception();
		return ex.getStackTrace()[callIndex].getClassName();
	}

}
