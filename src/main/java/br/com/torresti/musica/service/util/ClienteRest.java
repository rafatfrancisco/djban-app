package br.com.torresti.musica.service.util;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.MediaType;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.torresti.spring.exception.BusinessException;


public class ClienteRest<R> {

    private int conexaoTimeout = 120000;
    private int retornoTimeout = 120000;

    private static ObjectMapper LOGGER_MAPPER = new ObjectMapper();

    public R get(String url, Class<R> retorno, Object...parametros) throws BusinessException {
        return this.chamadaRest(HttpMethod.GET, url, null, null, retorno, null, parametros);
    }

    public R get(String url, Class<R> retorno, Map<String, ?> parametros) throws BusinessException {
        return this.chamadaRest(HttpMethod.GET, url, null, null, retorno, parametros);
    }

    public R get(String url, ParameterizedTypeReference<R> responseType, Object...parametros) throws BusinessException {
        return this.chamadaRest(HttpMethod.GET, url, null, responseType, null, null, parametros);
    }

    public R get(String url, ParameterizedTypeReference<R> responseType, Map<String, ?> parametros) throws BusinessException {
        return this.chamadaRest(HttpMethod.GET, url, null, responseType, null, parametros);
    }

    public R post(String url, Object entrada, Class<R> retorno, Object...parametros) throws BusinessException {
        return this.chamadaRest(HttpMethod.POST, url, entrada, null, retorno, null, parametros);
    }

    public R post(String url, Object entrada, Class<R> retorno, Map<String, ?> parametros) throws BusinessException {
        return this.chamadaRest(HttpMethod.POST, url, entrada, null, retorno, parametros);
    }

    public R post(String url, Object entrada, ParameterizedTypeReference<R> responseType, Object...parametros) throws BusinessException {
        return this.chamadaRest(HttpMethod.POST, url, entrada, responseType, null, null, parametros);
    }

    public R post(String url, Object entrada, ParameterizedTypeReference<R> responseType, Map<String, ?> parametros) throws BusinessException {
        return this.chamadaRest(HttpMethod.POST, url, entrada, responseType, null, parametros);
    }

    private R chamadaRest(HttpMethod metodo, String url, Object entrada, ParameterizedTypeReference<R> responseType, Class<R> retorno, Map<String, ?> parametroMap,  Object... parametros) throws BusinessException {

    	ResponseEntity<R> response = null;
        try {
        	
        	HttpHeaders httpHeaders = new HttpHeaders();
    		httpHeaders.setAccept(Arrays.asList(MediaType.ALL)); 
    		
    		HttpEntity<Object> request = new HttpEntity<>(entrada, httpHeaders);

            if ( parametroMap != null) {

            	if (responseType != null) {
            		response = this.getRestTemplate().exchange(url, metodo, request, responseType, parametroMap);
            	} else {
            		response = this.getRestTemplate().exchange(url, metodo, request, retorno, parametroMap);
            	}
            } else {

            	if (responseType != null) {
            		response = this.getRestTemplate().exchange(url, metodo, request, responseType, parametros);
            	} else {
            		response = this.getRestTemplate().exchange(url, metodo, request, retorno, parametros);
            	}
            }


        } catch (Exception e) {

        	try {
				new ObjectMapper().writeValueAsString(entrada);
			} catch (JsonProcessingException e1) {
				
				e1.printStackTrace();
			}
        	
        	logarChamadaRest(url, entrada, response, e, parametroMap != null ? parametroMap : parametros);

        	Map<String, String> metodoChamador = obterMetodoChamador();
        	throw new BusinessException(metodoChamador.get("servico") + " - " + metodoChamador.get("metodo"));
        }
        return response != null ? response.getBody() : null;
    }

    private Map<String, String> obterMetodoChamador() {

    	HashMap<String, String> retorno = new HashMap<>();

    	StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();

    	int i;
         for (i = 0; i < stackTraceElements.length; i++) {
         	if(stackTraceElements[i].getClassName().indexOf("ClienteRest") >= 0 && stackTraceElements[i].getMethodName().equals("get") || stackTraceElements[i].getMethodName().equals("post")) {
         		break;
         	}
         }

         StackTraceElement stackTraceElement = stackTraceElements[i+1];

         String className = stackTraceElement.getClassName();

         retorno.put("servico", className.substring(className.lastIndexOf(".") + 1).replace("Acessa", "").replace("Service", ""));
         retorno.put("metodo", stackTraceElement.getMethodName());

         return retorno;
    }

    public ClienteRest<R> configurarTimeout(int conexaoTimeout, int retornoTimeout) {
        this.conexaoTimeout = conexaoTimeout;
        this.retornoTimeout = retornoTimeout;
        return this;
    }

    public ClienteRest<R> multiplicarTimeout(int quantidadeItens) {

        this.configurarTimeout((this.conexaoTimeout * quantidadeItens), (this.retornoTimeout * quantidadeItens));

        return this;
    }

    public static RestTemplate getTemplate() {

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        RestTemplate restTemplate = new RestTemplate();

        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setObjectMapper(objectMapper);

        restTemplate.getMessageConverters().clear();
        restTemplate.getMessageConverters().add(converter);

        return restTemplate;
    }

    public RestTemplate getRestTemplate() {

        RestTemplate restTemplate = new RestTemplate();
        
        //Adiciona o TIMEOUT da chamada.
        SimpleClientHttpRequestFactory httpRequestFactory = (SimpleClientHttpRequestFactory) restTemplate.getRequestFactory();
        httpRequestFactory.setConnectTimeout(this.conexaoTimeout);
        httpRequestFactory.setReadTimeout(this.retornoTimeout);

        return restTemplate;
    }

    @Async
    public void logarChamadaRest(String acao, Object entrada, Object saida, Exception ex, Object parametros) {

        try {

            String entradaJson = (entrada == null ? null : LOGGER_MAPPER.writeValueAsString(entrada));

            String saidaJson = (saida == null ? null : LOGGER_MAPPER.writeValueAsString(saida));

            String parametrosJson = (parametros == null ? null : LOGGER_MAPPER.writeValueAsString(parametros));

            String error = ex != null ? "ERRO" : "INFO";

            @SuppressWarnings("unused")
			String mensagem = String.format("[%s - %s] ENTRADA [%s] SAIDA [%s] PARAMETROS [%s]", acao, error, entradaJson, saidaJson, parametrosJson);

            /*
            if (ex != null) {
                LOGGER.error(mensagem);
            } else {
                LOGGER.info(mensagem);
            }
            */

        } catch(Exception e) {
            //Ignora qualquer tipo de erro.
        }
    }
}
