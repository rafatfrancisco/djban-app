package br.com.torresti.musica.service.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.torresti.musica.crud.model.entidy.Participante;

@Repository
public interface ParticipanteRepository extends CrudRepository<Participante, Integer> {
	
	@Query("SELECT p FROM Participante p WHERE p.cpf = ?1")
	public Participante consultar(String cpf);

}
