package br.com.torresti.musica.service.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.torresti.musica.crud.model.entidy.PromocaoParticipante;

@Repository
public interface PromocaoParticipanteRepository extends CrudRepository<PromocaoParticipante, Integer> {
	
	@Query("SELECT p FROM Promocao x, PromocaoParticipante p WHERE x.idPromocao = ?1 AND p.promocao = x")
	public List<PromocaoParticipante> lista(Integer idPromocao);
	
	@Query("SELECT p FROM Promocao x, PromocaoParticipante p WHERE x.idPromocao = ?1 AND p.promocao = x AND p.numero = ?2")
	public List<PromocaoParticipante> consultar(Integer idPromocao, Integer numero);

}
