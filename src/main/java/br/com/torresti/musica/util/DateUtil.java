package br.com.torresti.musica.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.lang3.StringUtils;

/**
 * Classe que contem Metodo uteis para Data.<br>
 */
public final class DateUtil {
		
	public static Date parseData(String dt, String horaMinuto) throws ParseException {
		String[] tempo = horaMinuto.split(":");
		return parseData(dt, Integer.parseInt(tempo[0]), Integer.parseInt(tempo[1]));
	}
	
	public static Date parseData(String dt, Integer hora, Integer minuto) throws ParseException {
		Date data = parseData(dt);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);
		calendar.set(Calendar.HOUR_OF_DAY, hora);
		calendar.set(Calendar.MINUTE, minuto);
		return calendar.getTime();
	}
	
	/**
	 * Metodo que formata uma data como ddMMyyyy.<br>
	 * 
	 * @param data Data a ser formatada
	 * 
	 * @return String "ddMMyyyy"
	 */
	public static String formataSemHoraREST(Date data) {

		if (data == null) {
			return "";
		}

		return new SimpleDateFormat("ddMMyyyy").format(data);
	}
	
	/**
	 * Metodo que formata uma data como dd/MM/yyyy.<br>
	 * 
	 * @param data Data a ser formatada
	 * 
	 * @return String "dd/MM/yyyy"
	 */
	public static String formataSemHora(Date data) {

		if (data == null) {
			return "";
		}

		return new SimpleDateFormat("dd/MM/yyyy").format(data.getTime());
	}
	
	/**
	 * Metodo que formata data para YYYYDDMM.<br>
	 *
	 * @param data Data a ser formatada
	 *
	 * @return String contendo Date no formato YYYYMMDD
	 */
	public static  String formataDataYYYYMMDD(Date data) {
		
		if (data == null) {
			return "";
		}
		
		return new SimpleDateFormat("yyyyMMdd").format( data.getTime() );
	}
	
	/**
	 * Metodo que relaliza parse de um String no formado "DD/MM/YYYY" para um utiul.Date.<br> 
	 * 
	 * @param dt String Data a ser formatada
	 *  
	 * @return Date
	 * 
	 * @throws ParseException
	 */
	public static Date parseData(String dt) throws ParseException {

		if ( StringUtils.isEmpty( dt ) ) {
			return null;
		}
		
		return parse(dt, new SimpleDateFormat("dd/MM/yyyy") );
	}
	
	/**
	 * Metodo que relaliza parse de um String no formado "DD/MM/YYYY" para um utiul.Date.<br> 
	 * 
	 * @param dt String Data a ser formatada
	 *  
	 * @return Date
	 * 
	 * @throws ParseException
	 */
	public static Date parseDataDDMMYYYY(String dt) throws ParseException {
		
		if ( StringUtils.isEmpty( dt ) ) {
			return null;
		}

		return parse(dt, new SimpleDateFormat("ddMMyyyy") );
	}
	
	/**
	 * Metodo que reliza o parse de um string para um util.Date.<br> 
	 * 
	 * @param dt String Data a ser formatada
	 * @param df DateFormat 
	 * 
	 * @return Date
	 * 
	 * @throws ParseException
	 */
	private static Date parse(String dt, DateFormat df) throws ParseException {

		if (dt == null) {
			return null;
		}

		Date data = df.parse(dt);

		if (!df.format(data).equals(dt)) {
			throw new ParseException("Data invalida", 0);
		}

		return data;
	}

	
	/**
	 * Verifica se a data � logicamente v�lida, ou seja, se representa uma data real do calend�rio gregoriano (formato brasileiro).
	 *
	 * @param data String a ser validada, no formato "dd/MM/yyyy".
	 *
	 * @return true se for v�lida ou false caso contr�rio.
	 */
	public static boolean isValidDate(String data) {

		if (data == null) {
			return false;
		}
		if (!data.matches("\\d{2}[/-]?\\d{2}[/-]?\\d{4}")) {
			return false;
		}
		data = cleanup(data);
		int dia = Integer.parseInt(data.substring(0, 2));
		int mes = Integer.parseInt(data.substring(2, 4));
		int ano = Integer.parseInt(data.substring(4));
		if (dia < 1 || dia > 31) { // valida o dia
			return false;
		}
		if (mes < 1 || mes > 12) { // valida o Mes
			return false;
		}
		if ((mes == 4 || mes == 6 || mes == 9 || mes == 11) && dia == 31) {
			return false;
		}
		if (mes == 2) { // valida dia 29 de fevereiro
			if (dia > 29 || (dia == 29 && !isAnoBissexto(ano))) {
				return false;
			}
		}
		return true; // data v�lida
	}

	/**
	 * Retira os caracteres n�o nuMericos.
	 *
	 * @param data Data.
	 *
	 * @return Retorna uma nova String apenas com caracteres nuMericos (se houver).
	 */
	private static String cleanup(String data) {
		return data.replaceAll("\\D*", "");
	}

	/**
	 * Verifica se o ano informado � bissexto.
	 *
	 * @param ano Ano.
	 *
	 * @return true se for ano bissexto ou false caso contr�rio.
	 */
	public static boolean isAnoBissexto(int ano) {
		return (ano % 4 == 0 && (ano % 100 != 0 || ano % 400 == 0));
	}

	/**
	 * Truca a data/hora passada, retirando o hor�rio e atribuindo para zero o valor. Exemplo: Para a data/hora "01/01/2008 12:10:00.0", truncar e transforma o valor para "01/01/2008 00:00:00.0", ou seja, ignora o hor�rio.
	 *
	 * @param dt data a ser truncada.
	 *
	 * @return nova data.
	 */
	public static Date truncaData(Date dt) {
		if(dt == null) {
			return dt;
		}

		Calendar cal = Calendar.getInstance();
		cal.setLenient(false);
		cal.setTime(dt);
		GregorianCalendar gregCal = new GregorianCalendar(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
		return gregCal.getTime();
	}

	/**
	 * Retorna a diferenca em dias entra duas datas (posterior e anterior).
	 *
	 * @param depois Data mais tarde.
	 * @param antes Data mais cedo.
	 *
	 * @return Numero de dias da diferenca entra as datas.
	 */
	public static long getDiferencaEmDias(Date depois, Date antes) {

		long doze_horas = 43200000L; // ( 12 * 60 * 60 * 1000 ) copy core
		long vinte_horas = 86400000L; // ( 1000 * 3600 * 24 ) copy core

		long millis = depois.getTime() - antes.getTime();
		long diff = millis / vinte_horas;
		long mod = millis % vinte_horas;

		if (mod > doze_horas) {

			diff++;

		} else if (mod < (-1) * doze_horas) {

			diff--;
		}
		return diff;
	}

	/**
	 * Metodo que calcula uma nova data apartir de uma existente.<br>
	 *
	 * @param dt Data a ser calculada
	 * @param ano Ano a ser adicionado ou retirado
	 * @param mes Mes a ser adicionado ou retirado
	 * @param dia dia a ser adicionado ou retirado
	 *
	 * @return Nova data com basde nos parametro acima
	 */
	public static Date calculaNovaData(Date dt, int ano, int mes, int dia) {

		if (dt == null) {
			return null;
		}

		Calendar calendario = new GregorianCalendar();
		calendario.setLenient(false);
		calendario.setTime(dt);
		calendario.add(1, ano );
		calendario.add(2, mes );
		calendario.add(6, dia );
		calendario.add(10, 0);
		calendario.add(12, 0);
		calendario.add(13, 0);
		calendario.add(14, 0);

		return calendario.getTime();
	}

	/**
	 * Metodo que retorna o dia do Mes para data informada.<br>
	 *
	 * @param data Data
	 *
	 * @return int Dia do Mes
	 */
	public static int getDiaDoMes( Date data ) {

		if ( data == null ) {
			return 0;
		}

		return new Integer( new SimpleDateFormat("dd").format( data.getTime() ) );
	}

	/**
	 * Metodo que retorna o Mes para data informada.<br>
	 *
	 * @param data Data
	 *
	 * @return int  Mes
	 */
	public static int getMes(Date data) {

		if (data == null) {
			return 0;
		}

		return new Integer( new SimpleDateFormat("MM").format( data.getTime() ) );
	}
	
	/**
	 * Metodo que retorna o ano para data informada.<br>
	 *
	 * @param data Data
	 *
	 * @return int  Ano
	 */
	public static Integer getAno(Date data) {
		
		if (data == null) {
			return 0;
		}

		return new Integer( new SimpleDateFormat("yyyy").format( data.getTime() ) );
	}
	
	/**
	 * Metodo que retorna o Dia do Mes.<br>
	 * 
	 * @return String
	 */
	public static String getDiaDoMes() {

		Calendar hoje = new GregorianCalendar();

		return new SimpleDateFormat("dd").format(hoje.getTime());

	}
	
	/**
	 * Metodo que retorna a data formatada em "dd/MM/yyyy HH:mm:ss".<br>
	 * 
	 * @param date Data
	 * 
	 * @return String
	 */
	public static String formataCompleta(Date date) {

		if (date == null) { return ""; }

		return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(date);
	}
	
	/**
	 * Metodo que retorna a data formatada em "HH:mm".<br>
	 * 
	 * @param date Data
	 * 
	 * @return String
	 */
	public static String formataHoraMinuto(Date date) {

		if (date == null) { return ""; }

		return new SimpleDateFormat("HH:mm").format(date);
		
	}
	
	
}