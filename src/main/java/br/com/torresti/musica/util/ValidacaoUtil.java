package br.com.torresti.musica.util;

import br.com.torresti.spring.exception.BusinessException;
import br.com.torresti.spring.to.RestTO;
import br.com.torresti.spring.util.SituacaoRest;

public class ValidacaoUtil {

	public static void validar(Object valor, String mensagem) throws BusinessException {
		
		if (valor == null) {
			
			throw new BusinessException(mensagem);
			
		}
		 
	}
	
	public static void validar(String valor, String mensagem) throws BusinessException {
		
		if (StringUtil.isEmpty(valor)) {
			
			throw new BusinessException(mensagem);
			
		}
		 
	}
	
	public static void validar(RestTO restTO) throws BusinessException {
		
		if (restTO == null || SituacaoRest.Erro.getCodigo().equals(restTO.getCodigo())) {
			
			throw new BusinessException(restTO.getMensagem());
			
		}
		 
	}
	
	

}
