package br.com.torresti.musica.crud.model.entidy;

import java.io.Serializable;

/**
 * The Interface BaseEntity.
 */
public abstract interface BaseEntity extends Serializable {

}
