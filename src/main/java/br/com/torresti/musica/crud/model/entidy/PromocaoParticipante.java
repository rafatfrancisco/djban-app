package br.com.torresti.musica.crud.model.entidy;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@NamedQuery(name="PromocaoParticipante.findAll", query="SELECT t FROM PromocaoParticipante t")
public class PromocaoParticipante implements BaseEntity {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idPromocaoParticipante;
	
	@ManyToOne
	@JoinColumn(name="idPromocao")
	private Promocao promocao;
	
	@ManyToOne
	@JoinColumn(name="idParticipante")
	private Participante participante;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date data;
	
	private Integer numero;

	public Integer getIdPromocaoParticipante() {
		return idPromocaoParticipante;
	}

	public void setIdPromocaoParticipante(Integer idPromocaoParticipante) {
		this.idPromocaoParticipante = idPromocaoParticipante;
	}

	public Promocao getPromocao() {
		return promocao;
	}

	public void setPromocao(Promocao promocao) {
		this.promocao = promocao;
	}

	public Participante getParticipante() {
		return participante;
	}

	public void setParticipante(Participante participante) {
		this.participante = participante;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	
}
