<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!doctype html>
<html lang="en">
	<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Bootstrap CSS -->
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  	<link rel="stylesheet" href="/css/font-awesome.min.css">
  	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  	<script src="<c:url value='/js/geral/jquery-blockUI.js'/>"></script>
  	<script src="<c:url value='/js/geral/jquery-mask.js'/>"></script>
  	<script src="<c:url value='/js/geral/mascara.js'/>"></script>
  	<script src="<c:url value='/js/geral/main.js'/>"></script>
  	<title>DJ BAN</title>
</head>
<body>
	<div class="container">
		<br>
		<div class="row mensagens">
			<div class="col-md-12">
				<div class="alert alert-danger mensagem-erro" style="display:none">
					<a type="button" class="close" onclick="$(this).parent().hide();">&times;</a>
					<p class="txt-mensagem-erro"></p>
				</div>
				<div class="alert alert-success mensagem-sucesso" style="display:none">
					<a type="button" class="close" onclick="$(this).parent().hide();">&times;</a>
					<p class="txt-mensagem-sucesso"></p>
				</div>
			</div>
		</div>   		
		<br>
		<div class="corpo">	
			<script type="text/javascript">
				pageTransitionGet('${tela}', $('.corpo'));
			</script>
		</div>  		
	</div>
</body>
</html>