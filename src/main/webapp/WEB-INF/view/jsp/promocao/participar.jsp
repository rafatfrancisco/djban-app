<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<form:form id="frm" modelAttribute="participanteVO">

<input type="hidden" class="codigoPromocao" name="codigoPromocao" value="${participanteVO.codigoPromocao}"/>

<h3>Cadastro Participante</h3>
<hr>

<div class="row">
	<div class="col-12">
		<div class="form-group">
			<label for="email">Nome:</label>
		  	<input class="form-control" id="nome" name="nome" value="${participanteVO.nome}"  placeholder="Insira o nome">
		</div>
		<div class="form-group">
			<label for="email">E-mail:</label>
		  	<input class="form-control" id="email" name="email" value="${participanteVO.email}"  placeholder="Insira o email">
		</div>
		<div class="form-group">
			<label for="email">CPF:</label>
		  	<input class="form-control cpf" id="cpf" name="cpf" value="${participanteVO.cpf}"  placeholder="Insira o número do CPF">
		</div>
		<div class="form-group">
			<label for="email">Celular:</label>
		  	<input class="form-control celular" id="celular" name="celular" value="${participanteVO.celular}"  placeholder="Insira o celular">
		</div>
	</div>
</div>
<hr>
<div class="clearfix">
	<div class="pull-right">
		<input type="button" class="btn btn-success btn-cadastrar" value="Cadastrar">
	</div>
</div>

</form:form>

<script src="<c:url value='/js/page/promocao/participar.js'/>"></script>