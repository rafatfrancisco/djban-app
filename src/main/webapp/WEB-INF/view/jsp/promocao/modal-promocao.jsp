<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<form:form id="frmModal" modelAttribute="promocaoVO">
	<div class="modal fade" id="modalPromocao" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Promoção</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-6">
							<div class="form-group">
								<label for="email">Código:</label>
							  	<input class="form-control" id="codigo" name="codigo" value="${promocaoVO.codigo}"  placeholder="Insira o código">
							</div>
						</div>
						<div class="col-6">
							<div class="form-group">
								<label for="email">Nome:</label>
							  	<input class="form-control" id="nome" name="nome" value="${promocaoVO.nome}"  placeholder="Insira o nome">
							</div>
						</div>
						<div class="col-6">
							<div class="form-group">
								<label for="email">Data Inicio:</label>
							  	<input class="form-control" id="dataInicio" name="dataInicio" value="${promocaoVO.dataInicio}">
							</div>
						</div>
						<div class="col-6">						
							<div class="form-group">
								<label for="email">Hora Inicio:</label>
							  	<input class="form-control" id="horaInicio" name="horaInicio" value="${promocaoVO.horaInicio}">
							</div>
						</div>
						<div class="col-6">
							<div class="form-group">
								<label for="email">Data Fim:</label>
							  	<input class="form-control" id="dataFim" name="dataFim" value="${promocaoVO.dataFim}">
							</div>
						</div>
						<div class="col-6">
							<div class="form-group">
								<label for="email">Hora Fim:</label>
							  	<input class="form-control" id="horaFim" name="horaFim" value="${promocaoVO.horaFim}">
							</div>
						</div>
						<div class="col-6">						
							<div class="form-group">
								<label for="email">Data Sorteio:</label>
							  	<input class="form-control" id="dataSorteio" name="dataSorteio" value="${promocaoVO.dataSorteio}">
							</div>
						</div>
						<div class="col-6">
							<div class="form-group">
								<label for="email">Quantidade:</label>
							  	<input class="form-control" id="quantidade" name="quantidade" value="${promocaoVO.quantidade}">
							</div>
						</div>
						<div class="col-12">
							<div class="form-group">
								<label for="email">Descrição:</label>
							  	<input class="form-control" id="descricao" name="descricao" value="${promocaoVO.descricao}">
							</div>						
						</div>
					</div>				
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-salvar">Salvar</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
</form:form>
<script type="text/javascript">
$('.btn-salvar').on('click',function() {
	idPromocao = $('.idPromocao').val();
	$('#modalPromocao').modal('hide');
	if (idPromocao == '') {
		postAjaxPadrao('./promocao/salvar/ajax', 'frmModal');
	} else {
		postAjaxPadrao('./promocao/alterar/ajax', 'frmModal');
	}
});
</script>
