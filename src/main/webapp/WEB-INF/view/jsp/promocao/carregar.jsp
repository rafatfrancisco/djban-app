<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<form:form id="frm" modelAttribute="promocaoVO" >

<input type="hidden" class="idPromocao" name="idPromocao" value=""/>

<h3>Cadastro Promoção</h3>
<hr>

<br>
<div class="row">
	<div class="col-12">
		<table class="table table-dark">
			<thead>
				<tr>
					<th scope="col">Código</th>
					<th scope="col">Nome</th>
					<th scope="col">Inicio</th>
					<th scope="col">Fim</th>
					<th scope="col">Sorteio</th>
					<th scope="col">Ganhador</th>
					<th scope="col"></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="item" items="${promocaoVO.listaPromocao}" varStatus="status">
				<tr>
					<td>${item.codigo}</td>
					<td>${item.nome}</td>
					<td><fmt:formatDate type="date" pattern="dd/MM/yyyy HH:mm" value="${item.inicio}" /></td>
					<td><fmt:formatDate type="date" pattern="dd/MM/yyyy HH:mm" value="${item.fim}" /></td>
					<td><fmt:formatDate type="date" pattern="dd/MM/yyyy" value="${item.sorteio}" /></td>
					<td>${item.numero}</td>
					<td><a class="btn btn-success" onclick="consultar(${item.idPromocao});"><i class="fa fa-search" aria-hidden="true"></i></a></td>
					<td><a class="btn btn-primary" onclick="premiar(${item.idPromocao});"><i class="fa fa-search" aria-hidden="true"></i></a></td>
				</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>
<hr>
<div class="clearfix">
	<div class="pull-right">
		<input type="button" class="btn btn-success btn-incluir" value="Incluir">
	</div>
</div>

</form:form>

<div class="div-promocao"></div>

<div class="modal fade" id="modalPremio" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Informar Ganhador</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-12">
						<div class="form-group">
							<label for="email">Número:</label>
						  	<input class="form-control" id="numero" name="numero" value=""  placeholder="Insira o número do ganhador">
						</div>					
					</div>
				</div>				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-premiar">Premiar</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
			</div>
		</div>
	</div>
</div>

<script src="<c:url value='/js/page/promocao/carregar.js?30'/>"></script>