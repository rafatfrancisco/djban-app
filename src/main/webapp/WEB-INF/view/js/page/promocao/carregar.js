$(function() {
	colocarMascaras();
});

$('.btn-incluir').on('click', function() {
	getPadrao('./promocao/gerar', '.div-promocao', function () {
		setValor('.idPromocao', '');
		$('#modalPromocao').modal('show');	
	});
});

function consultar(idPromocao) {
	getPadrao('./promocao/consultar/' + idPromocao, '.div-promocao', function () {
		setValor('.idPromocao', idPromocao);	
		$('#modalPromocao').modal('show');	
	});
}

function premiar(idPromocao) {
	setValor('.idPromocao', idPromocao);
	setValor('#numero', '');
	$('#modalPremio').modal('show');
}

$('.btn-premiar').on('click',function() {
	idPromocao = $('.idPromocao').val();
	numero = $('#numero').val();
	$('#modalPremio').modal('hide');
	if (numero == '') {
		apresentarMensagemErro('Favor informar o número do ganhador');
	} else {
		getAjaxPadrao('./promocao/premiar/ajax/' + idPromocao + '/' + numero);		
	}
});

