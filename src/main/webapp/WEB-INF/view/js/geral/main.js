$.ajaxSetup({
	cache : false
});

$(document).ajaxStart(function() {
	$(document).off('keydown');
	$.blockUI({overlayCSS : {opacity : .5,backgroundColor : '#000'},css : {top : '300px'}});
}).ajaxStop(function() {
	$(document).on('keydown', function(e) {trataKeydown(e);});
	setTimeout($.unblockUI, 100);
}).ajaxComplete(function(event, request, settings) {
	setTimeout(function() {carregarMensagemTela();}, 900);
}).ajaxError(function(event, request, settings) {
	setTimeout(function() {apresentarMensagemErro(request.statusText);}, 900);
});

function getAjaxPadrao(url, sucessoCallback, erroCallback) {
	$.get(url, function(response) {
		tratarCallback(response, sucessoCallback, erroCallback);
	});	
}

function postAjaxPadrao(url, form, sucessoCallback, erroCallback) {
	$.post(url, serializeForm(form), function(response) {
		tratarCallback(response, sucessoCallback, erroCallback);
	});	
}

function tratarCallback(response, sucessoCallback, erroCallback) {
	if (response.codigo == 0) {
		apresentarMensagemSucesso(response.mensagem);
		if (sucessoCallback) {
			sucessoCallback();
		}
	} else {
		apresentarMensagemErro(response.mensagem);
		if (erroCallback) {
			erroCallback();
		}			
	}	
}

function getPadraoSimples(url) {
	getPadrao(url, ".corpo", null);
}

function getPadrao(url, local, sucessoCallback) {
	$.get(url, function(response) {
		getPostReturnPadrao(local, response, sucessoCallback);
	});	
}

function postPadraoSimples(url, form) {
	postPadrao(url, form, ".corpo", null);
}

function postPadrao(url, form, local, sucessoCallback) {
	$.post(url, serializeForm(form), function(response) {
		getPostReturnPadrao(local, response, sucessoCallback);
	});	
}

function getPostReturnPadrao(local, response, sucessoCallback) {
	$(local).html(response);
	if (sucessoCallback) {
		sucessoCallback();
	}	
}

function getValor(find) {
	$(find).val();
}

function setValor(find, value) {
	$(find).val(value);
}

function desabilitarSelect(find) {
	$(find).prop('disabled', 'disabled');
}

function carregarMensagemTela() {
	var mensagemSucesso = $('.mensagemSucesso').val();
	if (mensagemSucesso != null && mensagemSucesso != '') {
		apresentarMensagemSucesso(mensagemSucesso);
		$('.mensagemSucesso').val('');
	}
	var mensagemErro = $('.mensagemErro').val();
	if (mensagemErro != null && mensagemErro != '') {
		apresentarMensagemErro(mensagemErro);
		$('.mensagemErro').val('');
	}
}

function apresentarMensagemSucesso(texto) {
	if (texto != null && texto != '') {
		$('.mensagem-sucesso').show();
		$('.txt-mensagem-sucesso').html(texto);
	}
}

function apresentarMensagemErro(texto) {
	$('.mensagem-erro').show();
	$('.txt-mensagem-erro').html(texto);
}

function acessarMenu(opcao) {
	pageTransitionGet(opcao, $('.corpo'));
}

function pageTransitionGet(url, target, callback) {
	   $.get(url, function(response) {
			target.fadeOut(200, function() {
				target.html(response);
				target.fadeIn(800);
				if(callback){
					callback();
				}
			});
		});
}

function pageTransitionPost(url, idForm, target, callback) {
	$.post(url, serializeForm(idForm), function(response) {
		target.fadeOut(200, function() {
			target.html(response);
			target.fadeIn(800);
			if (callback) {
				callback();
			}
		});
	});
}

function serializeForm(idForm) {
	
	// Clona o form para remover as mascaras e submeter.
	var formSubmit = $('#' + idForm).clone();
		
	// Todos os campos do form Original
	var campos = $('#' + idForm).find(':input');

	// Todos os campos do form clonado
	var camposSubmit = formSubmit.find(':input');

	// Seleciona os campos que estão em divs ocultas.
	var camposDesabilitados = $('#' + idForm).find("div:hidden").find(":input");

	// Campos alterados dinamicamente (ajax) precisam ser copiados (o clone copia o valor original renderizado no HTML).
	for (var i = 0; i < campos.length; i++) {camposSubmit[i].value = campos[i].value;}

	// Remove as mascaras submetidas e removidas server-side);
	formSubmit.find(':input[type="text"]').filter('.moeda, .percentual, .cpf, .cnpj, .celular').each(function() {$(this).val($(this).maskMoney('unmasked')[0]);});
	
	return formSubmit.serialize();
	
}

function copiarTextoCrtlC(texto) {
	var textArea = document.createElement("textarea");
	textArea.style.position = 'fixed';
	textArea.style.top = 0;
	textArea.style.left = 0;
	textArea.style.width = '2em';
	textArea.style.height = '2em';
	textArea.style.padding = 0;
	textArea.style.border = 'none';
	textArea.style.outline = 'none';
	textArea.style.boxShadow = 'none';
	textArea.style.background = 'transparent';
	textArea.value = texto;
	document.body.appendChild(textArea);
	textArea.select();
	try {
		var successful = document.execCommand('copy');
		var msg = successful ? 'successful' : 'unsuccessful';
		console.log('Copying text command was ' + msg);
	} catch (err) {
		console.log('Oops, unable to copy');
		window.prompt("Copie para área de transferência: Ctrl+C e tecle Enter",texto);
	}
	document.body.removeChild(textArea);
}

function trataKeydown(e) {
	
	var keyCode = e.keyCode || e.which;

	if (keyCode == 9) {
		controlaTAB(e);
	} else if (e.keyCode == 8){
		var input = $(e.target).filter(":text, textarea, input[type=password], input[type=search]");
		
		if(input.exists()) {
			if (input.attr('readonly')) {
				e.preventDefault();
			}
		} else {
			e.preventDefault();
		}
	} else if (e.altKey) {
		
		var eventButton = null; 
		
		if (e.keyCode == 39) {
			eventButton = $(".btn-avancar, .btn-confirmar-proposta, .btn-transmitir-proposta, .btn-recalcular");
		} else if (e.keyCode == 37) {
			eventButton = $(".btn-voltar, .btn-cancelar-proposta");
		}
		
		if (eventButton != null) {
			$(":focus").change().blur();
			$('.modal:visible').modal('hide');
			setTimeout(function() {eventButton.click();}, 400);
			e.preventDefault();
		}
	} 
}