/**
 * Componente criado apenas para lidar com as Mascaras do sistema
 */


function mascaraCelular(event) {
	if(event != null && (event.which == 37 || (event.which == 8 && $(this).val().length == 3))) {
		return;
	}

	$(this).val($(this).val().replace(/\D/g, ""));

	var length = $(this).val().length;

	$(this).val($(this).val().replace(/^(\d{2})/, "($1)"));

	if(length <= 10) {
		$(this).val($(this).val().replace(/(\d{4})(\d)/, "$1-$2"));

	} else {
		$(this).val($(this).val().replace(/(\d{5})(\d{4})/, "$1-$2"));
	}
}

function mascaraCpfCnpj() {
	v_obj = $(this)[0];
	v_fun = cpfCnpj;
	setTimeout('execmascara()', 1);
}

function execmascara() {
	v_obj.value = v_fun(v_obj.value);
}

function cpfCnpj(v) {
	// Remove tudo o que não é dígito
	v = v.replace(/\D/g, "");
	if (v.length <= 11) { // CPF
		// Coloca um ponto entre o terceiro e o quarto dígitos
		v = v.replace(/(\d{3})(\d)/, "$1.$2");
		// Coloca um ponto entre o terceiro e o quarto dígitos
		// de novo (para o segundo bloco de números)
		v = v.replace(/(\d{3})(\d)/, "$1.$2");
		// Coloca um hífen entre o terceiro e o quarto dígitos
		v = v.replace(/(\d{3})(\d{1,2})$/, "$1-$2");
	} else { // CNPJ
		// Coloca ponto entre o segundo e o terceiro dígitos
		v = v.replace(/^(\d{2})(\d)/, "$1.$2");
		// Coloca ponto entre o quinto e o sexto dígitos
		v = v.replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3");
		// Coloca uma barra entre o oitavo e o nono dígitos
		v = v.replace(/\.(\d{3})(\d)/, ".$1/$2");
		// Coloca um hífen depois do bloco de quatro dígitos
		v = v.replace(/(\d{4})(\d)/, "$1-$2");
	}
	return v;
}

function removeNonDigits(value) {
	return value.replace(/\D/g, "");
}

function removeNonAlphanumeric(value) {
	return value.replace(/\W/g, "");
}

function colocarMascaras() {

	$(".cep").filter('input').mask("99999-999");

	$(".cpf").mask("999.999.999-99");
	
	$(".cnpj").mask("99.999.999/9999-99");

	$('.cpf-cnpj').on('paste keypress blur', mascaraCpfCnpj);
	
	$('.celular').on('keyup', mascaraCelular).keyup();

	$(".alphanumeric").filter('input').on('paste keypress blur', function() {
		campo = $(this);

		setTimeout(function () {
			campo.val(campo.val().toUpperCase().replace(/[^\w\s]/gi, ''));
		}, 1);
	});

	$(".cnh").mask("?99999999999", {placeholder:""});

	$(".data").filter('input').mask("99/99/9999");

	$('.moeda').filter('input').each(function() {
		
		if (parseFloat($(this).val()) == 0) {
			$(this).val(''); 
		}
		
		aceitaNegativo = false;
		if ($(this).hasClass('aceita-negativo')) {
			aceitaNegativo = true;
		}

		$(this).maskMoney({prefix : 'R$ ', allowNegative : aceitaNegativo, showSymbol : true, thousands : '.', decimal : ',', affixesStay : true });

		// não coloca a mascara se não tiver conteúdo. 
		//coloca mascara mesmo sem conteúdo apenas para os inputs contidos no Modal Conta Corrente
		if ($(this).val() != '' || $('.modal-conta-corrente').find(this).exists()) {
			$(this).val(parseFloat($(this).maskMoney('unmasked')[0]).toFixed(2)).maskMoney('mask');
		}
	});

	$(".numApolice").filter('input').mask("?99999999999999", {placeholder : ""});

	$('.percentual').filter('input').each(function() {

		$(this).maskMoney({ thousands : '', decimal : ',', allowZero : true, suffix : ' %' });

		if ($(this).val() != '') {
			$(this).val(parseFloat($(this).maskMoney('unmasked')[0]).toFixed(2)).maskMoney('mask');
		}
	});

	$(".placa").filter('input').mask("aaa-9999");

	//Transforma todos os campos para letras maiusculas.
	$("input[style='text-transform: uppercase;']").each(function () {
		$(this).val($(this).val().toUpperCase());
	});

	$("input[style='text-transform: uppercase;']").on('blur', function() {
		$(this).val($(this).val().toUpperCase());
	});

	//Permite que seja possivel a colagem de valores.
	$(".cep, .cpf-cnpj, .chassi, .data, .numApolice, .placa, .cnpj").not(":disabled").bind('paste', function() {
		if($(this).attr("disabled") != "disabled"){
		$(this).val('');
		} 
	});
}